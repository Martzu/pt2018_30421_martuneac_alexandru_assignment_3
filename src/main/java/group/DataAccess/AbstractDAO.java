package group.DataAccess;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Objects;

public class AbstractDAO<T> {

    private final Class<T> type;

    /**
     * Constructor, initiliazes type of data
     */
    public AbstractDAO()
    {
        this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     *
     * @param field
     * @return String
     */
    private String createSelectQuery(String field)
    {
        String query = "select * from " + type.getSimpleName() + " where " + field + " =?";
        return query;
    }

    /**
     *
     * @param resultSet
     * @return ArrayList<T></T>
     */
    private ArrayList<T> createObjects(ResultSet resultSet)
    {
            ArrayList<T> list = new ArrayList<>();
            try
            {
                while(resultSet.next())
                {
                    T instance = type.newInstance();
                    for(Field field : type.getDeclaredFields())
                    {
                        Object value = resultSet.getObject(field.getName());
                        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(),type);
                        Method method = propertyDescriptor.getWriteMethod();
                        method.invoke(instance,value);
                    }
                    list.add(instance);
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return list;
    }



    /**
     *
     * @param id
     * @return T
     */
    public T findById(int id)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try
        {
            connection = ConnectionFactory.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        }
        catch (Exception e)
        {

        }
        ConnectionFactory.getInstance().close(resultSet);
        ConnectionFactory.getInstance().close(statement);
        ConnectionFactory.getInstance().close(connection);
        return null;
    }

    /**
     *
     * @return ArrayList<T></T>
     */

    public ArrayList<T> findAll()
    {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "select * from " + type.getSimpleName();
        try
        {
            connection = ConnectionFactory.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        }
        catch(Exception e)
        {

        }
        ConnectionFactory.getInstance().close(resultSet);
        ConnectionFactory.getInstance().close(statement);
        ConnectionFactory.getInstance().close(connection);
        return null;
    }

    public void delete(int id)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = "delete from " + type.getSimpleName() +" where id = " + Integer.toString(id);
        try
        {
            connection = ConnectionFactory.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        }
        catch(Exception e)
        {

        }
        ConnectionFactory.getInstance().close(statement);
        ConnectionFactory.getInstance().close(connection);
    }

    /**
     *
     * @param object
     */

    public void insert(Object object)
    {
        boolean ok = false;
        String value = "";
        Connection connection = null;
        PreparedStatement statement = null;
        String query = "insert into " + type.getSimpleName() + " values(DEFAULT";
        for(Field x : object.getClass().getDeclaredFields())
        {
            x.setAccessible(true);
            if(ok == true)
            {
                try {
                    Object val = x.get(object);
                    value += ",";
                    value += "'";
                    value += val.toString();
                    value += "'";
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
            ok = true;
        }
        query += value;
        query += ")";
        System.out.println(query);
        try
        {
            connection = ConnectionFactory.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            statement.execute();
        }
        catch (Exception e)
        {

        }
        ConnectionFactory.getInstance().close(statement);
        ConnectionFactory.getInstance().close(connection);
    }


    /**
     *
     * @param object
     */
    public void update(Object object)
    {

        boolean ok = false;
        boolean comma = false;
        Connection connection = null;
        PreparedStatement statement = null;
        String query = "update " + type.getSimpleName() + " set ";
        String ID = " where ";
        String value = "";
        for(Field x : object.getClass().getDeclaredFields())
        {
            x.setAccessible(true);
            if(ok == false)
            {
                try
                {
                    Object val = x.get(object);
                    int index = x.toString().lastIndexOf(".");
                    String parameter = x.toString().substring(index + 1);
                    ID += parameter;
                    ID += "=";
                    ID += "'";
                    ID += val.toString();
                    ID += "'";
                } catch (IllegalAccessException e)
                {
                    e.printStackTrace();
                }
            }
            if(ok == true)
            {
                try
                {
                    Object val = x.get(object);
                    int index = x.toString().lastIndexOf(".");
                    String parameter = x.toString().substring(index + 1);
                    if(comma == true)
                    {
                        query +=",";
                    }
                    query += parameter;
                    query += "='";

                    query += val.toString();
                    query += "'";
                    comma = true;
                }
                catch (Exception e)
                {

                }

            }
            ok = true;
        }
        query += ID;
        try
        {
            connection = ConnectionFactory.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            statement.execute();
        }
        catch (Exception e)
        {

        }
        ConnectionFactory.getInstance().close(statement);
        ConnectionFactory.getInstance().close(connection);


    }






}
