package group.Model;

public class Product {

    private int id;
    private int price;
    private int quantity;

    /**
     *
     * @param id
     * @param price
     * @param quantity
     */
    public Product(int id, int price, int quantity)
    {
        this.quantity = quantity;
        this.id = id;
        this.price = price;
    }

    /**
     * Empty Constructor
     */
    public Product()
    {

    }

    /**
     *
     * @return int
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     */

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return int
     */
    public int getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     *
     * @return String
     */
    public String toString()
    {
        return Integer.toString(id) + " " + Integer.toString(price);
    }
}
