package group.Model;

public class Customer {
    private int id;
    private String name;
    private String address;
    private int age;


    /**
     *
     * @param id
     * @param name
     * @param address
     * @param age
     */
    public Customer(int id, String name, String address, int age)
    {
        this.id = id;
        this.name = name;
        this.address = address;
        this.age = age;
    }

    /**
     * Empty Constructor
     */
    public Customer()
    {

    }

    /**
     *
     * @return int
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @param age
     */

    public void setAge(int age) {
        this.age = age;
    }


    /**
     *
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return String
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return String
     */
    public String toString()
    {
        return Integer.toString(id) + " " + name + " " + address;
    }
}
