package group.BussinessLogic;

import group.DataAccess.ProductDAO;
import group.Model.Product;

import java.util.ArrayList;

public class ValidateProduct {

    private ProductDAO productDAO = new ProductDAO();
    private ArrayList<Product> products;

    /**
     * Constructor
     */
    public ValidateProduct()
    {
        products = new ArrayList<>();
        products = productDAO.findAll();
    }


    /**
     *
     * @param product
     * @return true
     */
    public boolean isValid(Product product)
    {
        for(Product x : products)
        {
            if(x.getId() == product.getId())
            {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param product
     * @return false
     */
    public boolean exists(Product product)
    {
        for(Product x : products)
        {
            if(x.getId() == product.getId())
            {
                return true;
            }
        }
        return false;
    }


    /**
     *
     * @param product
     * @return false
     */
    public boolean underStock(Product product)
    {
        if(product.getQuantity() < 10)
        {
            return true;
        }
        return false;
    }
}
