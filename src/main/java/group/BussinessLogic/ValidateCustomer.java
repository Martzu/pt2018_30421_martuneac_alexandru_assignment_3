package group.BussinessLogic;

import group.DataAccess.CustomerDAO;
import group.DataAccess.OrdersDAO;
import group.DataAccess.ProductDAO;
import group.Model.Customer;
import group.Model.Orders;
import group.Model.Product;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ValidateCustomer {

    private CustomerDAO customerDAO = new CustomerDAO();
    private OrdersDAO ordersDAO = new OrdersDAO();
    private ProductDAO productDAO = new ProductDAO();
    private ArrayList<Customer> customers;


    /**
     * validate Customers
     * no parameters
     */
    public ValidateCustomer()
    {
        customers = new ArrayList<>();
        customers = customerDAO.findAll();
    }

    /**
     *
     * @param customer
     * @return true
     */


    public boolean isValid(Customer customer)
    {
        for(Customer x : customers)
        {
            if (x.getId() == customer.getId())
            {
                return false;
            }
        }
        if(customer.getAge() < 18)
        {
            return false;
        }
        customerDAO.insert(customer);//afisez pe un jlabel, Invalid entry, ID already exists
        return true;
    }


    /**
     *
     * @param customer
     * @return false
     */
    public boolean exists(Customer customer)
    {
        for(Customer x : customers)
        {
            if(x.getId() == customer.getId())
            {
                return true;
            }
        }
        return false;
    }


    /**
     * creates bill for current Customer
     * @param customer
     */
    public void makeBill(Customer customer)
    {
        ArrayList<Orders> orders = new ArrayList<>();
        ArrayList<Product> products = new ArrayList<>();
        products = productDAO.findAll();
        orders = ordersDAO.findAll();
        int total = 0;
        int price = 0;
        int quantity = 0;
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("bill.txt");
            writer.println("Bill");
            writer.println("Name:" + customer.getName());
            writer.println("Address:" + customer.getAddress());
            for(Orders x : orders)
            {
                if(x.getCid() == customer.getId())
                {
                    for(Product y : products)
                    {
                        if (y.getId() == x.getPid())
                        {
                            //
                            total += y.getPrice()*x.getQuantity();
                            price = y.getPrice();
                            quantity = x.getQuantity();
                        }
                    }
                    writer.println("Product " + x.getPid() + " cost: "  + price + " quantity: " + quantity );
                }
            }
            writer.println("Total:" + total);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



}
