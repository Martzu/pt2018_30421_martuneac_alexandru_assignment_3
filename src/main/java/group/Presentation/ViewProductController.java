package group.Presentation;

import group.BussinessLogic.ValidateCustomer;
import group.BussinessLogic.ValidateProduct;
import group.DataAccess.CustomerDAO;
import group.DataAccess.ProductDAO;
import group.Model.Product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewProductController {

    ViewProduct viewProduct;

    /**
     * Constructor
     * @param view
     */
    public ViewProductController(ViewProduct view)
    {
        this.viewProduct = view;
        this.viewProduct.addCalculationListener(new CalculateListener());
    }

    class CalculateListener implements ActionListener
    {
        /**
         * Button Events
         * @param arg0
         */
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == viewProduct.getBack())
            {
                viewProduct.getFindID().setText("");
                viewProduct.dispose();
                View view = new View();
                ViewController controller = new ViewController(view);
                view.setVisible(true);

            }
            if(arg0.getSource() == viewProduct.getInsert())
            {
                viewProduct.getUnderStock().setText("");
                viewProduct.getFindID().setText("");
                ProductDAO productDAO = new ProductDAO();
                ValidateProduct customerInstance = new ValidateProduct();
                int price = Integer.parseInt(viewProduct.getPrice().getText());
                int quantity = Integer.parseInt(viewProduct.getQuantity().getText());
                Product product = new Product(0,price,quantity);
                System.out.println(product.toString());
                if(customerInstance.isValid(product))
                {
                    viewProduct.getMessage().setText("Successful");
                    productDAO.insert(product);
                }
                else
                {
                    viewProduct.getMessage().setText("");
                }
            }

            if(arg0.getSource() == viewProduct.getUpdate())
            {
                viewProduct.getUnderStock().setText("");
                viewProduct.getFindID().setText("");
                ProductDAO productDAO = new ProductDAO();
                ValidateProduct productInstance = new ValidateProduct();
                int id = Integer.parseInt(viewProduct.updateID.getText());
                int quantity = Integer.parseInt(viewProduct.updateQuantity.getText());
                int price = Integer.parseInt(viewProduct.updatePrice.getText());
                Product product = new Product(id,price,quantity);
                if(productInstance.exists(product))
                {
                    viewProduct.getMessage().setText("Update Sucessful");
                    productDAO.update(product);

                }
                else
                {
                    viewProduct.getMessage().setText("Product doesn't exist");
                }
            }

            if(arg0.getSource() == viewProduct.getDelete())
            {
                viewProduct.getFindID().setText("");
                viewProduct.getUnderStock().setText("");
                ProductDAO productDAO = new ProductDAO();
                ValidateProduct productInstance = new ValidateProduct();
                int id = Integer.parseInt(viewProduct.getDeleteID().getText());
                Product product = new Product(id,0,0);
                if(productInstance.exists(product))
                {
                    viewProduct.getMessage().setText("Delete Successful");
                    productDAO.delete(id);
                }
                else
                {
                    viewProduct.getMessage().setText("Deletion Unsuccessful");
                }
            }

            if(arg0.getSource() == viewProduct.getFindID())
            {
                ValidateProduct productInstance = new ValidateProduct();
                ProductDAO productDAO = new ProductDAO();
                try
                {
                    Product product = productDAO.findById(Integer.parseInt(viewProduct.getId().getText()));
                    viewProduct.getFoundById().setText(product.toString());
                    if(productInstance.underStock(product))
                    {
                        viewProduct.getUnderStock().setText("Understock");
                    }
                    else
                    {
                        viewProduct.getUnderStock().setText("");
                    }

                }
                catch (NullPointerException e)
                {
                    viewProduct.getFoundById().setText("Product doesn't exist");
                }
            }
            if(arg0.getSource() == viewProduct.getFindall())
            {

                ViewProduct viewProduct1 = new ViewProduct();
                ViewProductController controller1 = new ViewProductController(viewProduct1);
                viewProduct.dispose();
                viewProduct1.setVisible(true);
            }



        }
        }


    }
