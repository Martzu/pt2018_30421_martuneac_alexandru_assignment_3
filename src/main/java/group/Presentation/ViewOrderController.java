package group.Presentation;

import group.BussinessLogic.ValidateOrd;
import group.DataAccess.OrdersDAO;
import group.DataAccess.ProductDAO;
import group.Model.Orders;
import group.Model.Product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewOrderController {

    ViewOrder viewOrder;
    int ok;

    /**
     * Constructor
     * @param viewOrder
     */
    public ViewOrderController(ViewOrder viewOrder)
    {
        this.viewOrder = viewOrder;
        this.viewOrder.addCalculationListener(new CalculateListener());
        ok = 0;

    }

    public class CalculateListener implements ActionListener
    {
        /**
         * Button events
         * @param arg0
         */
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == viewOrder.getBack())
            {
                viewOrder.getFindID().setText("");
                viewOrder.dispose();
                View view = new View();
                ViewController controller = new ViewController(view);
                view.setVisible(true);

            }

            if(arg0.getSource() == viewOrder.getInsert())
            {
                viewOrder.getFindID().setText("");
                OrdersDAO ordersDAO = new OrdersDAO();
                ProductDAO productDAO = new ProductDAO();
                ValidateOrd orderInstance = new ValidateOrd();
                int product = Integer.parseInt(viewOrder.getProduct().getText());
                int customer = Integer.parseInt(viewOrder.getCustomer().getText());
                int quantity = Integer.parseInt(viewOrder.getQuantity().getText());
                Orders order = new Orders(0,customer,product,quantity);
                Product aux = productDAO.findById(product);
                Product product1 = new Product(product,aux.getPrice(),aux.getQuantity() - quantity);
                System.out.println(order.toString());
                if(orderInstance.isValid(order))
                {
                    viewOrder.getMessage().setText("Successful");
                    ordersDAO.insert(order);
                    productDAO.update(product1);

                }
                else
                {
                    viewOrder.getMessage().setText("Invalid Order");
                }
            }

            if(arg0.getSource() == viewOrder.getUpdate())
            {
                viewOrder.getFindID().setText("");
                OrdersDAO ordersDAO = new OrdersDAO();
                ValidateOrd orderInstance = new ValidateOrd();
                int id = Integer.parseInt(viewOrder.getUpdateID().getText());
                int product = Integer.parseInt(viewOrder.getUpdateProduct().getText());
                int customer = Integer.parseInt(viewOrder.getUpdateCustomer().getText());
                int quantity = Integer.parseInt(viewOrder.getUpdateQuantity().getText());
                Orders order = new Orders(id,customer,product,quantity);


                int previousValue;
                int currentValue;



                if(orderInstance.exists(order))
                {
                    viewOrder.getMessage().setText("Update Sucessful");
                    ordersDAO.update(order);

                }
                else
                {
                    viewOrder.getMessage().setText("Product doesn't exist");
                }
            }

            if(arg0.getSource() == viewOrder.getDelete())
            {
                viewOrder.getFindID().setText("");

                ValidateOrd orderInstance = new ValidateOrd();

                OrdersDAO ordersDAO = new OrdersDAO();

                int id = Integer.parseInt(viewOrder.getDeleteID().getText());

                Orders order = new Orders(id,0,0,0);

                if(orderInstance.exists(order))
                {
                    viewOrder.getMessage().setText("Delete Successful");
                    ordersDAO.delete(id);
                }
                else
                {
                    viewOrder.getMessage().setText("Deletion Unsuccessful");
                }
            }

            if(arg0.getSource() == viewOrder.getFindID())
            {
                ValidateOrd orderInstance = new ValidateOrd();
                OrdersDAO ordersDAO = new OrdersDAO();
                try
                {
                    Orders order = ordersDAO.findById(Integer.parseInt(viewOrder.getId().getText()));
                    viewOrder.getFoundById().setText(order.toString());

                }
                catch (NullPointerException e)
                {
                    viewOrder.getFoundById().setText("Product doesn't exist");
                }
            }

            if(arg0.getSource() == viewOrder.getFindall())
            {

                ViewOrder viewOrder1 = new ViewOrder();
                ViewOrderController controller1 = new ViewOrderController(viewOrder1);
                viewOrder.dispose();
                viewOrder1.setVisible(true);
            }
        }
    }
}
