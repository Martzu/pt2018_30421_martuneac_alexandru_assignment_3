package group.Presentation;

import group.BussinessLogic.ValidateCustomer;
import group.DataAccess.CustomerDAO;
import group.Model.Customer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

public class ViewCustomerController {

    ViewCustomer viewCustomer;
    ArrayList<Customer> customers;

    /**
     * Constructor
     * @param viewCustomer
     */
    public ViewCustomerController(ViewCustomer viewCustomer)
    {
        this.viewCustomer = viewCustomer;
        this.viewCustomer.addCalculationListener(new CalculateListener());


    }

    class CalculateListener implements ActionListener
    {
        /**
         * Button Events
         * @param arg0
         */
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == viewCustomer.getBack())
            {
                viewCustomer.getFindID().setText("");
                viewCustomer.dispose();
                View view = new View();
                ViewController controller = new ViewController(view);
                view.setVisible(true);

            }
            if(arg0.getSource() == viewCustomer.getInsert())
            {
                viewCustomer.getFindID().setText("");
                ValidateCustomer customerInstance = new ValidateCustomer();
                String name = viewCustomer.getNam().getText();
                String address = viewCustomer.getAddress().getText();
                int age = Integer.parseInt(viewCustomer.getAge().getText());
                Customer customer = new Customer(0,name,address,age);
                System.out.println(customer.toString());
                if(customerInstance.isValid(customer))
                {
                    viewCustomer.getMessage().setText("Successful");
                }
                else
                {
                    viewCustomer.getMessage().setText("Not old enough");
                }
            }
            if(arg0.getSource() == viewCustomer.getBill())
            {
                viewCustomer.getFindID().setText("");
                ValidateCustomer customerInstance = new ValidateCustomer();
                int id = Integer.parseInt(viewCustomer.getBillId().getText());
                String name = viewCustomer.getBillName().getText();
                String address = viewCustomer.getBillAddress().getText();
                int age = Integer.parseInt(viewCustomer.getBillAge().getText());
                Customer customer = new Customer(id,name,address,age);
                if(customerInstance.exists(customer))
                {
                    viewCustomer.getMessage().setText("Bill for current customer created");
                    customerInstance.makeBill(customer);
                }
                else
                {
                    viewCustomer.getMessage().setText("Customer doesn't exist");
                }
            }

            if(arg0.getSource() == viewCustomer.getUpdate())
            {
                viewCustomer.getFindID().setText("");
                CustomerDAO customerDAO = new CustomerDAO();
                ValidateCustomer customerInstance = new ValidateCustomer();
                int id = Integer.parseInt(viewCustomer.getUpdateId().getText());
                String name = viewCustomer.getUpdateName().getText();
                String address = viewCustomer.getUpdateAddress().getText();
                int age = Integer.parseInt(viewCustomer.getUpdateAge().getText());
                Customer customer = new Customer(id,name,address,age);
                if(customerInstance.exists(customer))
                {
                    viewCustomer.getMessage().setText("Update Sucessful");
                    customerDAO.update(customer);

                }
                else
                {
                    viewCustomer.getMessage().setText("Customer doesn't exist");
                }
            }

            if(arg0.getSource() == viewCustomer.getDelete())
            {
                viewCustomer.getFindID().setText("");
                CustomerDAO customerDAO = new CustomerDAO();
                ValidateCustomer customerInstance = new ValidateCustomer();
                int id = Integer.parseInt(viewCustomer.getDeleteID().getText());
                Customer customer = new Customer(id,"a","b",16);
                if(customerInstance.exists(customer))
                {
                    viewCustomer.getMessage().setText("Delete Successful");
                    customerDAO.delete(id);
                }
                else
                {
                    viewCustomer.getMessage().setText("Deletion Unsuccessful");
                }
            }

            if(arg0.getSource() == viewCustomer.getFindID())
            {
                CustomerDAO customerDAO = new CustomerDAO();
                try
                {
                    Customer customer = customerDAO.findById(Integer.parseInt(viewCustomer.getId().getText()));
                    viewCustomer.getFoundById().setText(customer.toString());
                }
                catch (NullPointerException e)
                {
                    viewCustomer.getFoundById().setText("User doesn't exist");
                }
            }
            if(arg0.getSource() == viewCustomer.getFindall())
            {

                ViewCustomer viewCustomer1= new ViewCustomer();
                ViewCustomerController controller1 = new ViewCustomerController(viewCustomer1);
                viewCustomer.dispose();
                viewCustomer1.setVisible(true);
            }



        }
    }
}
