package group.Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame{

    private JButton customer = new JButton("Customers");
    private JButton orders = new JButton("Orders");
    private JButton products = new JButton("Products");

    /**
     * Start main window
     */
    public View()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600,800);
        this.setVisible(true);

        c.insets = new Insets(10,10,10,10);

        panel.add(customer,c);

        c.gridy = 1;
        panel.add(orders,c);

        c.gridy = 2;
        panel.add(products,c);

        this.add(panel);

    }

    /**
     * Adds listeners for buttons
     * @param listenForCalcButton
     */

    public void addCalculationListener(ActionListener listenForCalcButton)
    {
        customer.addActionListener(listenForCalcButton);
        products.addActionListener(listenForCalcButton);
        orders.addActionListener(listenForCalcButton);
    }


    /**
     *
     * @return JButton
     */
    public JButton getCustomer() {
        return customer;
    }

    /**
     *
     * @return JButton
     */

    public JButton getOrders() {
        return orders;
    }

    /**
     *
     * @return JButton
     */

    public JButton getProducts() {
        return products;
    }


}
